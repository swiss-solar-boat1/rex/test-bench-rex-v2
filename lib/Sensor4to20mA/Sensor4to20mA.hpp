/*  Written by Baptiste Savioz
 *  May 2023
 *  
 *  Library made for generic 4-20 mA sensors
 */

#ifndef SENSOR4TO20MA_HPP
#define SENSOR4TO20MA_HPP

#include <CAN.h>

class Sensor4to20mA
{
    public:
    Sensor4to20mA(int dataPin, bool debug = false, bool printMeasurmentEnable = true, float resistance = 150); // constructeur
    void init();
    bool makeMeasurement() ;
    void attachCANInstance(CANSAME5x* canInstance, int sensorCAN_ID);
    void sendRawMeasurementToCAN();
    void sendMeasurementToCAN();
    float lastMeasurement = 0;
    float lastValue=0;
 
    protected:
    CANSAME5x* _canInstance = nullptr;
    int _sensorCAN_ID = 0;
    int _dataPin = 0;
    bool _debug = false;
    bool _printMeasurmentEnable = true;
    float _resistance;
};


class Pressure : public Sensor4to20mA
{
    public :
    Pressure(int dataPin, bool debug = false, bool printMeasurmentEnable = true, float resistance = 150);
    bool makeMeasurement();
};

class Flowmeter : public Sensor4to20mA
{
    public :
    Flowmeter(int dataPin, bool debug = false, bool printMeasurmentEnable = true, float resistance = 150);
    bool makeMeasurement();
};

#endif // SENSOR4TO20MA_HPP