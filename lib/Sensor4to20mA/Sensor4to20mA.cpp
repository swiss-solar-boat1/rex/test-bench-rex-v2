/*  Written by Baptiste Savioz
 *  May 2023
 *  
 *  Library made for generic 4-20 mA sensors
 */


#include <Sensor4to20mA.hpp>
#include <Arduino.h>


#define INCR_TO_VOLT 3.3/1023
#define AMP_TO_MILI_AMP 1000

/**
 * Constructor of the sensor object
 * @param dataPin Physical pin on the MCU where the sensor is connected
 * @param setpointPin (optional, default none) Physical pin to provide setpoint to sensor if available 
 * @param debug (optional, default false) Enable all debug print
 * @param printMeasurementEnable (optional, default True) print measurement made to Serial (most useful debug print)
 * @param resistance optinal parameter one can provide to improve precision, default 160 Ohm, must never exceed 165 Ohm
*/
Sensor4to20mA::Sensor4to20mA(int dataPin, bool debug, bool printMeasurmentEnable, float resistance){
    _dataPin = dataPin;
    _debug = debug;
    _printMeasurmentEnable = printMeasurmentEnable;
    _resistance = resistance;
}


/**
 * Initialise the data pin correctly as an input
*/
void Sensor4to20mA::init(){
    // Setup correct Pin Mode
    pinMode(_dataPin, INPUT);
}


/**
 * make measurement and store it in lastMeasurement
 * Unit of lastMeasurement are milliAmps
 * @return boolean according if measurement is in valid range or not
*/
bool Sensor4to20mA::makeMeasurement(){
    uint32_t temp;
    temp = analogRead(_dataPin);

    if(_debug){
        Serial.print("Raw measurement :");
        Serial.println(temp);
    }

    lastMeasurement = temp * INCR_TO_VOLT * AMP_TO_MILI_AMP /_resistance; // measurement in milliAmp

    if(_debug){
        Serial.print("measurement [mA] : ");
        Serial.println(lastMeasurement);
    }

    return (4.0 <= lastMeasurement) && (lastMeasurement <= 20.0);
}


/**
 * Attach an CAN instance (CAN0 or CAN1) to the sensor, where it could send its measurement if needed
*/
void Sensor4to20mA::attachCANInstance(CANSAME5x* canInstance, int sensorCAN_ID){
    _canInstance = canInstance;
    _sensorCAN_ID = _sensorCAN_ID;
}


/**
 * Send the raw measurement to the attached CAN with the given ID
*/
void Sensor4to20mA::sendRawMeasurementToCAN(){
    _canInstance->beginPacket(_sensorCAN_ID);
    _canInstance->write(lastMeasurement);
    _canInstance->endPacket();
}


/**
 * Send the measurement to the attached CAN with the given ID
*/
void Sensor4to20mA::sendMeasurementToCAN(){
    _canInstance->beginPacket(_sensorCAN_ID);
    _canInstance->write(lastMeasurement);
    _canInstance->endPacket();
}

Pressure ::Pressure(int dataPin, bool debug , bool printMeasurmentEnable , float resistance)
                     : Sensor4to20mA(dataPin,debug,printMeasurmentEnable,resistance){}


bool Pressure ::makeMeasurement(){
    bool temp=Sensor4to20mA::makeMeasurement();
    lastValue=(lastMeasurement-4)/16 * 6 ; //in bar
    return temp;
}


 Flowmeter ::Flowmeter(int dataPin, bool debug, bool printMeasurmentEnable, float resistance)
                     : Sensor4to20mA(dataPin,debug,printMeasurmentEnable,resistance){}

bool Flowmeter ::makeMeasurement(){
    bool temp=Sensor4to20mA::makeMeasurement();

    lastValue=50*(lastMeasurement-4)/16; // in Ln/min
    return temp;
}