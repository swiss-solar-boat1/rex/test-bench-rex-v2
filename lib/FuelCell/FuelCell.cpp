/*  Written by Baptiste Savioz
 *  May 2023
 *  
 *  Library made to read CAN data from the sunlaite fuel cell
 */
#include <FuelCell.hpp>

//CAN id in decimal for the Fuel Cell 
#define CURRENT_ID 100
#define VOLTAGE_ID 120
#define TEMPERATURE_ID 180

#define TEMP_SCALE_FACTOR 0.1 // [deg C/incr] 
#define CURRENT_SCALE_FACTOR 0.1 // [Amp/incr] 
#define VOLT_SCALE_FACTOR 0.1 // [Volt/incr] 

// init static 
CANSAME5x* FuelCell::_canInstanceRead = nullptr;
CANSAME5x* FuelCell::_canInstanceRepeat = nullptr;
float FuelCell::temperature = 0;
float FuelCell::current = 0;
float FuelCell::voltage = 0;
bool FuelCell::_shouldPrintMeasurement = false;
bool FuelCell::_shouldRepeatOnCan = true;
bool FuelCell::_debug = false;


/**
 * Attach a CAN instance (CAN0 or CAN1) to the object, where it could read the fuel cell data
*/
void FuelCell::attachCANInstanceToRead(CANSAME5x* canInstance){
    _canInstanceRead = canInstance;
}


/**
 * Attach a CAN instance (CAN0 or CAN1) to the object, where it could send its measurement if needed
*/
void FuelCell::attachCANInstanceToRepeat(CANSAME5x* canInstance){
    _canInstanceRepeat = canInstance;
}

void FuelCell::readData(int packetLength){

    uint16_t MSB = 0x0000;
    uint16_t LSB = 0x0000;

    // Read the packet ID
    long canIDReceived = _canInstanceRead->packetId();

    switch (canIDReceived)
    {
    case CURRENT_ID:
        // Check that payload has the correct length
        if(packetLength != 2){
            if(_debug){Serial.println("Oupsi, not a correct fuel cell frame");}
            return;
        }

        LSB = _canInstanceRead->read(); 
        MSB = _canInstanceRead->read();

        current = ((MSB << 8) + LSB) * CURRENT_SCALE_FACTOR;

        if(_debug || _shouldPrintMeasurement){
            Serial.print("Current : ");
            Serial.print(voltage);
            Serial.print("[Amp], data : ");
            Serial.print(MSB);
            Serial.print(LSB);
            Serial.println(" ");
            }
        break;

    case VOLTAGE_ID:
        // Check that payload has the correct length
        if(packetLength != 2){
            if(_debug){Serial.println("Oupsi, not a correct fuel cell frame");}
            return;
        }

        LSB = _canInstanceRead->read(); 
        MSB = _canInstanceRead->read();

        voltage = ((MSB << 8) + LSB) * VOLT_SCALE_FACTOR;

        if(_debug || _shouldPrintMeasurement){
            Serial.print("Voltage : ");
            Serial.print(voltage);
            Serial.print("[V], data : ");
            Serial.print(MSB);
            Serial.print(LSB);
            Serial.println(" ");
            }
        if(_shouldRepeatOnCan){repeatOnCAN(canIDReceived, MSB, LSB);}
        break;

    case TEMPERATURE_ID:
        // Check that payload has the correct length
        if(packetLength != 2){
            if(_debug){Serial.println("Oupsi, not a correct fuel cell frame");}
            return;
        }

        LSB = _canInstanceRead->read(); 
        MSB = _canInstanceRead->read();

        temperature = ((MSB << 8) + LSB) * TEMP_SCALE_FACTOR;

        if(_debug || _shouldPrintMeasurement){
            Serial.print("Temperature : ");
            Serial.print(voltage);
            Serial.print("[Deg C], data : ");
            Serial.print(MSB);
            Serial.print(LSB);
            Serial.println(" ");
            }
        break;
    
    default:
        if(_debug){
            Serial.print("Oupsi, unknown fuel cell frame... Packet ID :");
            Serial.print(canIDReceived);
            Serial.print(_canInstanceRead->read());
            while(_canInstanceRead->available()){ // To debuff
                Serial.print(_canInstanceRead->read());
            }
        }else{
            while(_canInstanceRead->available()){ // To debuff
                Serial.print(_canInstanceRead->read());
            }
        }
        break;
    }

    if(_shouldRepeatOnCan){repeatOnCAN(canIDReceived, MSB, LSB);}
}

void FuelCell::repeatOnCAN(int messageID, uint16_t MSB, uint16_t LSB){
    _canInstanceRepeat->beginPacket(messageID);
    _canInstanceRepeat->write(LSB);
    _canInstanceRepeat->write(MSB);
    _canInstanceRepeat->endPacket();
}