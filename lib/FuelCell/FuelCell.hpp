/*  Written by Baptiste Savioz
 *  May 2023
 *  
 *  Library made to read CAN data from the sunlaite fuel cell
 */

#ifndef FUELCELL_HPP
#define FUELCELL_HPP

#include <CAN.h>

class FuelCell
{
    public:
    static void readData(int packetLength);
    void attachCANInstanceToRead(CANSAME5x* canInstance);
    void attachCANInstanceToRepeat(CANSAME5x* canInstance);
    static float temperature;
    static float current;
    static float voltage;
 
    private:
    static CANSAME5x* _canInstanceRead;
    static CANSAME5x* _canInstanceRepeat;
    static bool _shouldPrintMeasurement;
    static bool _shouldRepeatOnCan;
    static bool _debug;
    static void repeatOnCAN(int messageID, uint16_t MSB, uint16_t LSB);
};

#endif // FUELCELL_HPP