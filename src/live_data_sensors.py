# Program to read and display the values sent on Serial by the MCU

# - A PuTTy session should be opened and configure to print in the file 
#   log.csv (tutorial to configure the PuTTy session 
#             https://www.circuitbasics.com/logging-arduino-data-to-files-on-a-computer/)
# - This program should not be launched in the first 10s (wait for the first 100 points)

import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib import style
from matplotlib.animation import FuncAnimation
import pandas as pd
import numpy as np

#initialize the plots
fig, (ax1,ax2) = plt.subplots(2, 1)


# Counts the number of line in the csv file without reading it
def get_num_lines(fname):
    with open(fname) as f:
        for i, _ in enumerate(f):
            pass
    return i + 1

x=np.linspace(0,99, 100)


#Reads the last 100 data points (last 10s) of the csv file 
#First column = Flowmeter
#Second column = Pressure
#Called everytime that the graph refreshes (every 500ms)
def update(frame):
   num_lines = get_num_lines("log.csv")
   n = 102
   y = pd.read_csv("log.csv", skiprows=range(1,num_lines-n))

   y=y.to_numpy()
   y=np.delete(y, slice(100,102), 0)
   if len(y[:,0]) > 100 :
       y=np.delete(y, slice(100,len(y)), 0)

   ax1.clear()
   ax2.clear()

   #Setting all the information/scales of the 2 plots
   ax1.set_ylim(0, 3)
   ax2.set_ylim(0, 65)


   ax1.set_title("Pression")
   ax2.set_title("Flowmeter")

   ax1.set_xlabel("Time : last 10s [s/10]")
   ax2.set_xlabel("Time : last 10s [s/10]")

   ax1.set_ylabel("Pression [bar]")
   ax2.set_ylabel("Débit [Ln/min]")


   z=y[:,0]
   y=np.delete(y,0,1)
   # Constante horizontale en y=2
   ax1.axhline(y=2, color='r', linestyle='-')
   ax1.plot(x,y,'g-')
   # Constante horizontale  e y=55
   ax2.axhline(y=55, color='r', linestyle='-')
   ax2.plot(x,z,'g-')
   return (ax1,ax2)

animation = FuncAnimation(fig, update, interval=500)
fig.tight_layout()
plt.show()