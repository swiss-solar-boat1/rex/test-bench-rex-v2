#include <Arduino.h>
#include <CAN.h>
#include <Sensor4to20mA.hpp>
#include <FuelCell.hpp>

//--------------------- DEFINE --------------------------------
// Flow meter 
#define R_FLOWMETER_SIGNAL                                  150
#define PIN_FLOWMETER_DATA                            PIN0_ADC0
#define FLOWMETER_CAN_ID                                     42

// Pressure sensor
#define R_PRESSURE_SIGNAL                                   150
#define PIN_PRESSURE_DATA                             PIN3_ADC0
#define PRESSURE_CAN_ID                                      52

// Helper
#define MEASURMENT_PERIOD_MS                                100
#define BLINK_PERIOD_MS                                     500
#define BLINK_LED                                          PB05





//--------------- Function declaration ------------------------
void debuffCAN0(int packetlength);


//---------------- GLOBAL VARIABLES ---------------------------
Flowmeter flowmeter(PIN_FLOWMETER_DATA, false, true, R_FLOWMETER_SIGNAL);
Pressure pressure(PIN_PRESSURE_DATA, false, true, R_PRESSURE_SIGNAL);
FuelCell fuelCell;
bool debug = false;
bool ledState = false;
unsigned long lastBlink = 0;
unsigned long lastMeasurementTime = 0;
char str[30];

//---------------------- SETUP -------------------------------
void setup() {

    // start the serial communication to communicate with the computer
    Serial.begin(9600);
    if(debug){Serial.println("Starting up...");}

    // Initalise the sensors
    flowmeter.init();

    // Initialise CAN1 - fuel cell dedicated CAN
    if(!can1.begin(250E3)){
        if(debug){Serial.println("Starting CAN1 failed");}
    }else{
       if(debug){ Serial.println("Starting CAN1 Succed");}
        fuelCell.attachCANInstanceToRead(&can1);
        can1.onReceive(fuelCell.readData); // attach readData function to the receive callback
    }

    // Initialise CAN0 - log dedicated CAN
    if(!can0.begin(1000E3)){
        if(debug){Serial.println("Starting CAN0 failed");}
    }else{
        if(debug){Serial.println("Starting CAN0 Succed");}
        can0.onReceive(debuffCAN0);
        fuelCell.attachCANInstanceToRepeat(&can0);
        flowmeter.attachCANInstance(&can0, FLOWMETER_CAN_ID);
    }

    // Setup the blink LED
    pinMode(BLINK_LED, OUTPUT);  
}


//----------------------- LOOP -------------------------------
void loop() {
    // Blink
    if(millis() > lastBlink + BLINK_PERIOD_MS) {
        lastBlink = millis();
        ledState = !ledState;
        digitalWrite(BLINK_LED, ledState);
    }

    // Sensor measurment
    if(millis() > (lastMeasurementTime + MEASURMENT_PERIOD_MS)){
        
        flowmeter.makeMeasurement();
        pressure.makeMeasurement();     //always called in second to achieve the right format for csv
        
        sprintf(str, "%f , %f",flowmeter.lastValue,pressure.lastValue);
        
        Serial.println(str);  

       lastMeasurementTime=millis();
    }
}

void debuffCAN0(int packetlength){
    while(can0.available()){
        if(debug){Serial.println("debuffing");}
        can0.read();
    }
}
